import { Component } from '@angular/core';
interface Login{
  firstname:string,
  secondname:string
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  login:Login={
    firstname:'',
    secondname:''
  }
  numbers:number=2;
  // plusboolean=false;
  // minusboolean=false;
  table=[];
  add(){
    this.table.push({...this.login})
    console.log(this.login)
  }
  remove(i){
    this.table.splice(i,1)
  }
  plus(){
    this.numbers=this.numbers+1;
  }
  minus(){
    this.numbers=this.numbers-1;
  }
}
